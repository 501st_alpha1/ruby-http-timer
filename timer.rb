#!/usr/bin/env ruby

require 'net/http'

MINUTES = 5

if ARGV[0]
  domain = ARGV[0]
else
  domain = 'gitlab.com'
end

print "Timing HTTP responses from " + domain + "...\n"

times = 0
startTime = Time.now
endTime = startTime + (60 * MINUTES)
while Time.now < endTime do
  Net::HTTP.get(domain, '/')
  print '.'
  times = times + 1
end

print "\n"

totalTime = endTime - startTime
averageTime = totalTime / times

print "Total number of requests was " + times.to_s + "\n"
print "Average time was " + averageTime.to_s + " seconds\n"
